<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    protected $table = 'product_colors';

    protected $with = ['windows', 'color'];

    public function color(){
        return $this->hasOne(Colors::class,'id', 'color_id');
    }

    public function windows(){
        return $this->hasMany(ProductWindow::class, 'color_id', 'id');
    }
}
