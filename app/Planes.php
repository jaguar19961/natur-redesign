<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planes extends Model
{
    protected $table = 'planes';


    public function lang(){
        return $this->hasOne(PlaneLangs::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(PlaneLangs::class, 'article_id', 'id');
    }
}
