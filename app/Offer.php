<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';
    protected $with = ['lang'];
    public function lang(){
        return $this->hasOne(OfferLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function formDetail(){
        return $this->hasOne(Form::class, 'id', 'form_id');
    }
}
