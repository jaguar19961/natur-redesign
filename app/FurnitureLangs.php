<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FurnitureLangs extends Model
{
    protected $table = 'furniture_langs';

    public function category(){
        return $this->belongsTo(Furnitures::class, 'id');
    }
}
