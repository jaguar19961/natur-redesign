<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerieFurniture extends Model
{
    protected $table = 'serie_furniture';

    public function furnit(){
        return $this->hasOne(Planes::class, 'id', 'furniture_id');
    }
}
