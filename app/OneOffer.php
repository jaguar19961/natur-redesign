<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneOffer extends Model
{
    protected $table = 'one_offers';
}
