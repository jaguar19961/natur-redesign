<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpSystem extends Model
{
    protected $table = 'open_systems';

    protected $with = ['gallery', 'gallerysec'];

    public function gallery()
    {
        return $this->hasMany(OpGallery::class, 'op_id', 'id')->where('position', 1);
    }

    public function gallerysec()
    {
        return $this->hasMany(OpGallery::class, 'op_id', 'id')->where('position', 2);
    }
}
