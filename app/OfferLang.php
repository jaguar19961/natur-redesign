<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferLang extends Model
{
    protected $table = 'offer_langs';
}
