<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogLang extends Model
{
    protected $table = 'blog_langs';
}
