<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFurniture extends Model
{
    protected $table = 'product_furniture';

    public function show(){
        return $this->hasMany(Furnitures::class,'id', 'furniture_id');
    }
}
