<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoLang extends Model
{
    protected $table = 'seo_langs';
}
