<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryDropdown;
use Illuminate\Http\Request;

class MenuEditorController extends Controller
{
    public function menuEditor(){
        $menues = Category::get();
        return view('admin.partials.menu-editor', compact('menues'));
    }

    public function submenuSave(Request $request){

//        dd($request->all());
        $sub = CategoryDropdown::find($request->id);
        $sub->link = $request->link;
        $sub->save();
        return redirect()->back();
    }
}
