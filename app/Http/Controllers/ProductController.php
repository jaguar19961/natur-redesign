<?php

namespace App\Http\Controllers;

use App\ProductNew;
use App\ProductPopular;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function productAddToNew($id){

            $new = new ProductNew();
            $new->product_id = $id;
            $new->save();



        return redirect()->back();
    }

    public function productDeleteToNew($id){
        $new = ProductNew::where('product_id', $id);
        $new->delete();

        return redirect()->back();
    }

    public function productAddToPopular($id){


            $new = new ProductPopular();
            $new->product_id = $id;
            $new->save();

        return redirect()->back();
    }

    public function productDeleteToPopular($id){

        $new = ProductPopular::where('product_id', $id);
        $new->delete();
        return redirect()->back();
    }
}
