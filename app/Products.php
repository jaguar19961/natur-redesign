<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Products extends Model
{
    protected $table = 'products';

    protected $with = ['windows','colors'];


    public function lang(){
        return $this->hasOne(ProductLangs::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function serie(){
        return $this->hasOne(Serie::class,'id', 'serie_id');
    }

    public function colors(){
        return $this->hasMany(ProductColor::class,'product_id', 'id');
    }

    public function color(){
        return $this->hasOne(ProductColor::class,'product_id', 'id');
    }

    public function furnitures(){
        return $this->hasMany(ProductFurniture::class,'product_id', 'id');
    }

    public function planes(){
        return $this->hasMany(ProductPlane::class,'product_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'id' );
    }

    public function categories(){
        return $this->hasOne(Category::class,'id', 'category_id');
    }

    public function popular(){
        return $this->hasOne(ProductPopular::class,'product_id', 'id');
    }

    public function productnew(){
        return $this->hasOne(ProductNew::class,'product_id', 'id');
    }

    public function langs(){
        return $this->hasOne(ProductLangs::class, 'article_id', 'id');
    }

    public function style(){
        return $this->hasOne(Style::class, 'style_id', 'id');
    }

    public function windows(){
        return $this->hasManyThrough(
            'App\ProductWindow',
            'App\ProductColor',
            'product_id',
            'color_id'
            );
    }
}
