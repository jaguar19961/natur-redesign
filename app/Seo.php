<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seos';

    public function lang(){
        return $this->hasOne(SeoLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(SeoLang::class, 'article_id', 'id');
    }
}
