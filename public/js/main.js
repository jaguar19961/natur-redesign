$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });

  /* play btn */
  $('.play-btn').click(function() {
    $('.modal.header_video').addClass('is-active ');
  })
  $('.modal-close').click(function name() {
    $('.modal.header_video').removeClass('is-active ');
  });

  /* product color */
  $('.color_block').click(function () {

    var colorLabel = $(this).data('color-label');
    var imgSrc = $(this).data('img-src');

    $('.color_block').removeClass('is_active')
    $(this).toggleClass('is_active');
    $('.text_color').text(colorLabel);

    $('.product_img img').attr({
      'src': imgSrc
    })
  });

  /* Tabs */
  $('.tabs a').each(function() {
    $(this).click(function(e) {
      e.preventDefault();
      var target = $(this).data('target');
      $('.tabs a').removeClass('is_active');
      $(this).addClass('is_active');
      $('.tab_panel').removeClass('is_active');
      $(target).addClass('is_active');
    })
  })
});
