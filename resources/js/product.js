// window._ = require('lodash');

window.onscroll = function() {scrollFunction(); myFunction(); myFunction2();};

function myFunction() {
    var navbar = document.getElementById("navbar");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
            navbar.classList.remove("is-transparent");
            navbar.classList.add("is-white");
        } else {
            navbar.classList.remove("is-fixed-top");
            navbar.classList.add("is-transparent");
            navbar.classList.remove("is-white");
        }
    }

}

function myFunction2() {
    var navbar = document.getElementById("navbar2");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
        } else {
            navbar.classList.remove("is-fixed-top");
        }
    }

}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("to_top").style.opacity = 1;
    } else {
        document.getElementById("to_top").style.opacity = 0;
    }
}

function loadData() {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, 2000);
    })
}

loadData()
    .then(() => {
        let preloaderEl = document.getElementById('preloader');
        preloaderEl.classList.add('hidden');
        preloaderEl.classList.remove('visible');
    });

require('./bulma-extensions');

document.addEventListener('DOMContentLoaded', function () {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});
require('./bootstrap');
window.Vue = require('vue');

import './theme/element-variables.scss';
import {
    Dropdown,
    DropdownMenu,
    DropdownItem,
    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    Upload} from 'element-ui';

Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Upload);

import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper, /* { default global options } */)


Vue.component('color-window', require('./components/ColorWindow.vue').default);
Vue.component('product-series', require('./components/ProductSeries.vue').default);
Vue.component('product-page-new', require('./components/product/ProductPage.vue').default);
Vue.component('color-window-new', require('./components/product/ColorWindow.vue').default);

Vue.component('collection-edit', require('./components/admin/CollectionEdit.vue').default);
Vue.component('carach', require('./components/Characteristics.vue').default);

import CookieLaw from 'vue-cookie-law'

const app = new Vue({
    el: '#app',
    components: { CookieLaw },
    data: {
        activeTab: 'complect',
        serch: false,
        preloader: true,
        swiperOption:{
            slidesPerView: 6,
            spaceBetween: 30,
            freeMode: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
                waitForTransition: false,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            breakpoints:{
                1408: {
                    slidesPerView: 6,
                    spaceBetween: 40
                },
                1216: {
                    slidesPerView: 5,
                    spaceBetween: 30
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 30
                },
                769: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        },
        colors: [],
        ferestre: [],
        color_selected: {},
        selected_f: null,
        loading: false,
        description: true
    },
    methods: {
        topFunction(){
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        },
        selectImage(color, windows, fereastra){
            this.color_selected = color;
            this.ferestre = windows;
            this.selected_f = fereastra;
        },
        LoadData(e){
            //this.loading = e
        },
        updateFerestra(e){
            this.selected_f = e;
        }
    },
    computed: {
        textShow(){
            if(this.description == true){
                return 'Află mai multe detalii despre această ușă'
            }else{
                return 'Ascunde informatiile adaugatoare'
            }
        },
        curImg () {
            return this.selected_f
        }
    },
    mounted(){
        setTimeout(() => {
            this.preloader = false
        }, 100);

    }
});


// Bulma NavBar Burger Script

