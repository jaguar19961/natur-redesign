@extends('front.layouts.new-white-nav')
@section('content')

    <section class="offer_show">
        <div class="section-top">
            <h1>{{$item->lang->name}}</h1>
            <div class="top-detail">
                <span><img src="/img/clock.png" alt="">{{$item->created_at->format('d F, Y')}}</span>
                @if($is_blog)
                <span><img src="/img/keyboard.png" alt="">Mariana Melnic</span>
                @endif
                <span><img src="/img/folder.png" alt="">{{$item->lang->keywords}}</span>
            </div>
        </div>
        <div class="container section-content @if($item->form_id == 0) is-ful-with @else is-sidebar @endif">
            <div>
                <img src="/images/article/{{$item->image_offer}}" alt="">
                @if($item->lang != null)
                {!! $item->lang->description !!}
                @endif
            </div>
            @if($item->form_id != 0)
            <sidebar>
                <div class="card-offert-form">
                    <h3 class="form-title">PRIMEȘTE OFERTA ACUM!</h3>
                    {{ Form::open(array('method' => 'put','action' => 'FrontController@getPhone')) }}
                    {{ csrf_field() }}
                        <div class="form-input">
                            <input class="input" type="text" placeholder="Nume" name="name">
                        </div>
                        <div class="form-input">
                            <input class="input" type="tel" placeholder="Telefon" name="phone">
                        </div>
                        <button type="submit" class="button is-primary btn-primary">Trimite</button>
                    {{Form::close()}}
                </div>
            </sidebar>
            @endif
        </div>
    </section>

@endsection