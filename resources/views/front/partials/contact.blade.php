@extends('front.layouts.new-white-nav')
@section('content')
    <section id="contact_page">
        <div class="section-name page-title"><h1 class="section-sub-title"><span>Contacte</span></h1></div>
        <div class="container section-conten">
            <div class="col">
               <div>
                   <h1 class="section-name-map"><span>Chisinau</span></h1>
                   <iframe src="{{$infos->go}}"
                           width="100%" height="250" frameborder="0" allowfullscreen style="height: 200px !important;"></iframe>
               </div>
            </div>
            <div class="col">
                <h2 class="fs-35">Contactează-ne</h2>
                <p>Te rugăm să ne contactezi dacă ai întrebări sau ai nevoie de asistență. Îți oferim cu drag toate informațiile necesare și suport.</p>
                {{ Form::open(array('method' => 'put','action' => 'FrontController@getColaboration')) }}
                {{ csrf_field() }}
                <div class="columns">
                    <div class="column"><input class="input" type="text" placeholder="Nume" name="nume"></div>
                </div>
                <div class="columns">
                    <div class="column pr-9"><input class="input" type="tel" placeholder="Telefon" name="telefon"></div>
                    <div class="column pl-9"><input class="input" type="email" placeholder="Email" name="email"></div>
                </div>
                <div class="columns">
                    <div class="column"><input class="input" type="text" placeholder="Subiect" name="subiect"></div>
                </div>
                <div class="columns">
                    <div class="column"><textarea class="textarea" placeholder="Mesaj" name="mesaj"></textarea></div>
                </div>
                <button type="submit" class="button is-primary btn-primary">Trimite</button>
                {{Form::close()}}
            </div>
        </div>
    </section>

@endsection
