@extends('front.layouts.new')
@section('content')
    <section class="section" id="section_header_sisteme">
        <img src="{{asset('images/systems/'.$content->image)}}" alt="example" class="top_img">

        <div class="section-header-content">
            <h1>{{$content->name}}</h1>
            <a href="#nav_sistem">
                <img src="/img/chevron-down.png" alt="">
            </a>
        </div>
    </section>
    @if($content->id == 13)
    <section id="nav_sistem">
        <ul>
            <li><a href="#sec_1" class="sistem-link">PEREȚI DESPĂRȚITORI DE STICLĂ</a></li>
            <li><a href="#sec_2" class="sistem-link">Proiecte mici</a></li>
            <li><a href="#sec_3" class="sistem-link">sistem de închidere</a></li>
            <li><a href="#sec_4" class="sistem-link">POSIBILITĂȚI LA MAXIM</a></li>
        </ul>
    </section>
    @guest()
    <section id="sec_1" class="section-ancor section-50">
        <div class="container section-content">
            <div class="col">
                <div class="section-name section-name-50 d-flex-vertical-align-center h-100">
                    <h1 class="section-sub-title">
                        <span>{!! $content->sc1_title !!}</span><br>
                        {!! $content->sc1_desc !!}
                    </h1>
                    <p>{!! $content->sc1_text !!}</p>
                </div>
            </div>
            <div class="col sis-sec-img"><img src="{{asset('images/site/'.$content->sc1_img)}}" alt="home"></div>
        </div>
    </section>
    <section id="sec_2" class="section-ancor section-50 pb0">
        <div class="container section-content">
            <div class="col sis-sec-img"><img src="{{asset('images/site/'.$content->sc2_img)}}" alt="home"></div>
            <div class="col">
                <div class="section-name section-name-50 d-flex-vertical-align-center h-100">
                    <h1 class="section-sub-title">
                        <span>{!! $content->sc2_title !!}</span><br>
                        {!! $content->sc2_desc !!}
                    </h1>
                    {!! $content->sc2_text !!}
                </div>
            </div>
        </div>
    </section>

    <carousel-sistem :idsec="{{json_encode($content->gallery)}}"></carousel-sistem>

    <section id="sec_4" class="sistem-section-primary">
        <div class="container">
            <h3>Acesta sunt doar câteva variații standard posibile. Dar specialiștii Profil Doors procesează comenzi personalizate așa încât limită poate fi doar imaginația ta de designer sau anumite mecanisme rare, însă chiar și așa nu ezita să te inspiri din lucrările altor constructori și designeri. </h3>
        </div>
    </section>

    <carousel-sistem :idsec="{{json_encode($content->gallerysec)}}"></carousel-sistem>
    @else
    <collection-edit :sistem="{{json_encode($content)}}"></collection-edit>
    @endguest
    @else
        <section id="sec_1" class="section-ancor">
            <div class="container mt-20">
                {!! $content->content !!}
            </div>
        </section>
    @endif

@endsection

@section('script')
    <script>


        window.addEventListener("scroll", event => {
            let mainNavLinks = document.getElementsByClassName("sistem-link");
            let mainSections = document.getElementsByClassName("section-ancor");
            let lastId;
            let cur = [];
            let fromTop = window.scrollY;
            if(mainNavLinks.length > 0){
                var i;
                for (i = 0; i < mainNavLinks.length; i++){
                    let section = document.querySelector(mainNavLinks[i].hash);
                    if(section != null){
                        if (
                            section.offsetTop <= fromTop &&
                            section.offsetTop + section.offsetHeight > fromTop
                        ) {
                            mainNavLinks[i].classList.add("current");
                        } else {
                            mainNavLinks[i].classList.remove("current");
                        }
                    }

                }
            }
        });
    </script>
@endsection