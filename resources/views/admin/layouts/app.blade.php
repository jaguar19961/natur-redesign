

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="images/favicon/favicon.ico">

    <title>Natur - Admin panel</title>

    @include('admin.layouts.css')



    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">

                        {{--<img alt="image" class="rounded-circle" src="images/adminlogo/{{Auth::user()->images}}" style="width:50px; height:50px"/>--}}
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            @if (Auth::guest())
                                <a href="{{ route('login') }}">Login</a>
                                <a href="{{ route('register') }}">Register</a>
                            @else
                                <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                            @endif
                            <span class="text-muted text-xs block">Administrator</span>
                        </a>
                        {{--<ul class="dropdown-menu animated fadeInRight m-t-xs">--}}
                            {{--<li><a class="dropdown-item" href="{{ url('/profile') }}">Profil</a></li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="">
                    <a href="#" aria-expanded="false">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Adaugare usi</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                        <li class="{{ request()->is('admin/home') ? 'active' : '' }}">
                            <a href="{{ url('/admin/home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Adauga usi</span></a>
                        </li>
                        <li class="{{ request()->is('admin/edit_serie') ? 'active' : '' }}">
                            <a href="{{ url('/admin/edit_serie') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Serie</span></a>
                        </li>
                        <li class="{{ request()->is('admin/edit_color') ? 'active' : '' }}">
                            <a href="{{ url('/admin/edit_color') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Culori</span></a>
                        </li>
                        <li class="{{ request()->is('admin/edit_window') ? 'active' : '' }}">
                            <a href="{{ url('/admin/edit_window') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Insertii</span></a>
                        </li>
                        <li class="{{ request()->is('admin/edit_furniture') ? 'active' : '' }}">
                            <a href="{{ url('/admin/edit_furniture') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Feronerie</span></a>
                        </li>
                        <li class="{{ request()->is('admin/edit_molding') ? 'active' : '' }}">
                            <a href="{{ url('/admin/edit_molding') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Molding</span></a>
                        </li>
                    </ul>
                </li>
                <li class="{{ request()->is('admin/open-system') ? 'active' : '' }}">
                    <a href="{{ url('/admin/open-system') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Sisteme de deschidere</span></a>
                </li>
                <li class="{{ request()->is('admin/parchet') ? 'active' : '' }}">
                    <a href="{{ url('/admin/parchet') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Adaugare Parchet</span></a>
                </li>
                <li class="{{ request()->is('admin/edit_product') ? 'active' : '' }}">
                    <a href="{{ url('/admin/edit_product') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Editare Produse</span></a>
                </li>
                <li class="{{ request()->is('admin/edit_home_page') ? 'active' : '' }}">
                    <a href="{{ url('/admin/edit_home_page') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Pagina principala</span></a>
                </li>
                {{--<li class="{{ request()->is('admin/gallery') ? 'active' : '' }}">--}}
                    {{--<a href="{{ url('/admin/gallery') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Galerie</span></a>--}}
                {{--</li>--}}
                <li class="{{ request()->is('admin/blog') ? 'active' : '' }}">
                    <a href="{{ url('/admin/blog') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Blog</span></a>
                </li>
                <li class="{{ request()->is('admin/offer') ? 'active' : '' }}">
                    <a href="{{ url('/admin/offer') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Oferte</span></a>
                </li>
                <li class="{{ request()->is('admin/seo') ? 'active' : '' }}">
                    <a href="{{ url('/admin/seo') }}"><i class="fa fa-th-large"></i> <span class="nav-label">SEO</span></a>
                </li>
                <li class="{{ request()->is('admin/menu-editor') ? 'active' : '' }}">
                    <a href="{{ url('/admin/menu-editor') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Menu Editor</span></a>
                </li>
                <li class="{{ request()->is('admin/forms') ? 'active' : '' }}">
                    <a href="{{ url('/admin/forms') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Formulare</span></a>
                </li>
            </ul>

        </div>
    </nav>

{{--    @if ($validator->fails())--}}
{{--        <span class="text-danger">{{ $errors }}</span>--}}
{{--    @endif--}}
{{--    if ($validator->fails()) {--}}
{{--    $error = $validator->errors()->first();--}}
{{--    }--}}
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li style="padding: 20px">
                        <span class="m-r-sm text-muted welcome-message">  </span>
                    </li>

                    <!-- Authentication Links -->

                    <li class="nav-item dropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">


                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>


                </ul>

            </nav>
        </div>
        <br>
        @if(session()->get('errors'))
            <span class="text-danger">{{ session()->get('errors')->first() }}</span>
        @endif
        @yield('content')

        {{--@include('admin.layouts.cssjs')--}}
        <br>
        <br>
        <div class="footer">
            <div class="float-right">

            </div>
            <div>
                <strong>Copyright</strong> midavco.com &copy; 2019
            </div>
        </div>
    </div>



</div>


@include('admin.layouts.js')
@include('admin.layouts.script-js')

</body>


</html>
