<link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<!-- Toastr style -->
<link href="{{asset('admin/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

<!-- Gritter -->
<link href="{{asset('admin/js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">


<link href="{{asset('admin/css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/cropper/cropper.min.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/switchery/switchery.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/nouslider/jquery.nouislider.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/ionRangeSlider/ion.rangeSlider.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/clockpicker/clockpicker.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">

<link href="{{asset('admin/css/plugins/dualListbox/bootstrap-duallistbox.min.css')}}" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- lincuri file input -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">