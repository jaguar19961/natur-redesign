{{--pentru tinimce + afisare fisier selectat si selec 2 controller--}}
<script>
    $('.custom-file-input').on('change', function () {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html('selectata');
    });
</script>

<script>
    $(document).ready(function () {
        $("#id1_1").select2({width: '100%', closeOnSelect: false, placeholder: 'Alege Culorile'});
    });
</script>
<script>
    $(document).ready(function () {
        $("#id1_2").select2({width: '100%', closeOnSelect: false, placeholder: 'Alege furnitura',});
    });
</script>
<script>
    $(document).ready(function () {
        $("#id1_3").select2({width: '100%', closeOnSelect: false, placeholder: 'Alege molding'});
    });
</script>
<script>
    $(document).ready(function () {
        $("#id1_4").select2({width: '100%', closeOnSelect: false, placeholder: 'Alege sticla'});
    });
</script>

<script src="{{URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>

    var editor_config = {
        path_absolute: "{{ url('/') }}/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "sizeselect | fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>


{{--extragere categorii subcategorii si sub--}}
<script>
    $('#category_id').on('change', function () {
        getProducts($(this).val());
    });

    function getProducts(category_id) {
        $.get("{{url('/admin/category/sub')}}/" + category_id, function (data) {
            $("#subcategory_id").html(data);
        });
    }

    $(document).ready(function () {
        getProducts($('#category_id').val());
    });
</script>

<script>
    $('#subcategory_id').on('change', function () {
        getProducts1($(this).val());
    });

    function getProducts1(subcategory_id) {
        $.get("{{url('/admin/category/sub/sub')}}/" + subcategory_id, function (data) {
            $("#subsubcategory_id").html(data);
        });
    }

    $(document).ready(function () {
        getProducts1($('#subcategory_id').val());
    });
</script>

{{--calculare procent si suma totala de vinzare pentru adaugarea unui produs--}}
<script>
    $(function () {

        $('#selling_price').on('input', function () {
            calculate();
        });
        $('#comercial_price').on('input', function () {
            calculate();
        });
        $('#total_price').on('input', function () {
            calculate1();
        });

        function calculate1() {
            var pPos = parseFloat($('#selling_price').val());
            var pEarned = " ";
            var perc = parseFloat($('#total_price').val());


            if (isNaN(pPos) || isNaN(perc)) {
                pEarned = " ";
            } else {
                pEarned = ((perc / pPos * 100) - 100).toFixed(2).replace(/,/g, '.');
            }

            $('#comercial_price').val(pEarned);
        }

        function calculate() {
            var pPos = parseFloat($('#selling_price').val());
            var pEarned = parseInt($('#comercial_price').val());
            var perc = " ";

            if (isNaN(pPos) || isNaN(pEarned)) {
                perc = " ";
            } else {
                perc = ((pPos * pEarned / 100) + pPos).toFixed(2).replace(/,/g, '.');
            }

            $('#total_price').val(perc);
        }

    });
</script>

{{--calculare preti si procent pentru un pallet--}}

<script>
    $(function () {

        $('#selling_price_pallet').on('input', function () {
            calculate2();
        });
        $('#comercial_price_pallet').on('input', function () {
            calculate2();
        });
        $('#total_price_pallet').on('input', function () {
            calculate3();
        });

        function calculate3() {
            var pPos = parseFloat($('#selling_price_pallet').val());
            var pEarned = " ";
            var perc = parseFloat($('#total_price_pallet').val());


            if (isNaN(pPos) || isNaN(perc)) {
                pEarned = " ";
            } else {
                pEarned = ((perc / pPos * 100) - 100).toFixed(2).replace(/,/g, '.');
            }

            $('#comercial_price_pallet').val(pEarned);
        }

        function calculate2() {
            var pPos = parseFloat($('#selling_price_pallet').val());
            var pEarned = parseInt($('#comercial_price_pallet').val());
            var perc = " ";

            if (isNaN(pPos) || isNaN(pEarned)) {
                perc = " ";
            } else {
                perc = ((pPos * pEarned / 100) + pPos).toFixed(2).replace(/,/g, '.');
            }

            $('#total_price_pallet').val(perc);
        }

    });
</script>


<script>
    $(document).ready(function () {
        // setTimeout(function () {
        //     toastr.options = {
        //         closeButton: true,
        //         progressBar: true,
        //         showMethod: 'slideDown',
        //         timeOut: 4000,
        //         preventDuplicates: true,
        //     };
        //     toastr.success('', 'Welcome to Admin');
        //
        // }, 1300);

        var sparklineCharts = function () {
            $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 52], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });

            $("#sparkline2").sparkline([32, 11, 25, 37, 41, 32, 34, 42], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });

            $("#sparkline3").sparkline([34, 22, 24, 41, 10, 18, 16, 8], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1C84C6',
                fillColor: "transparent"
            });
        };

        var sparkResize;

        $(window).resize(function (e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineCharts, 500);
        });

        sparklineCharts();


        var data1 = [
            [0, 4], [1, 8], [2, 5], [3, 10], [4, 4], [5, 16], [6, 5], [7, 11], [8, 6], [9, 11], [10, 30], [11, 10], [12, 13], [13, 4], [14, 3], [15, 3], [16, 6]
        ];
        var data2 = [
            [0, 1], [1, 0], [2, 2], [3, 0], [4, 1], [5, 3], [6, 1], [7, 5], [8, 2], [9, 3], [10, 2], [11, 1], [12, 0], [13, 2], [14, 8], [15, 0], [16, 0]
        ];
        $("#flot-dashboard5-chart").length && $.plot($("#flot-dashboard5-chart"), [
                data1, data2
            ],
            {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#d5d5d5'
                },
                colors: ["#1ab394", "#1C84C6"],
                xaxis: {},
                yaxis: {
                    ticks: 4
                },
                tooltip: false
            }
        );

        // var doughnutData = {
        //     labels: ["App", "Software", "Laptop"],
        //     datasets: [{
        //         data: [300, 50, 100],
        //         backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
        //     }]
        // };


        // var doughnutOptions = {
        //     responsive: false,
        //     legend: {
        //         display: false
        //     }
        // };


        // var ctx4 = document.getElementById("doughnutChart").getContext("2d");
        // new Chart(ctx4, {type: 'doughnut', data: doughnutData, options: doughnutOptions});

        // var doughnutData = {
        //     labels: ["App", "Software", "Laptop"],
        //     datasets: [{
        //         data: [70, 27, 85],
        //         backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
        //     }]
        // };


        // var doughnutOptions = {
        //     responsive: false,
        //     legend: {
        //         display: false
        //     }
        // };


        // var ctx4 = document.getElementById("doughnutChart2").getContext("2d");
        // new Chart(ctx4, {type: 'doughnut', data: doughnutData, options: doughnutOptions});

    });
</script>

<script>
    $(document).ready(function () {

        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });

        var $image = $(".image-crop > img")
        $($image).cropper({
            aspectRatio: 1.618,
            preview: ".img-preview",
            done: function (data) {
                // Output the result data for cropping image.
            }
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
            $inputImage.change(function () {
                var fileReader = new FileReader(),
                    files = this.files,
                    file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage.val("");
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#download").click(function () {
            window.open($image.cropper("getDataURL"));
        });

        $("#zoomIn").click(function () {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function () {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function () {
            $image.cropper("rotate", 45);
        });

        $("#rotateRight").click(function () {
            $image.cropper("rotate", -45);
        });

        $("#setDrag").click(function () {
            $image.cropper("setDragMode", "crop");
        });

        var mem = $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        var yearsAgo = new Date();
        yearsAgo.setFullYear(yearsAgo.getFullYear() - 20);

        $('#selector').datepicker('setDate', yearsAgo);


        $('#data_2 .input-group.date').datepicker({
            startView: 1,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('#data_3 .input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $('#data_4 .input-group.date').datepicker({
            minViewMode: 1,
            keyboardNavigation: false,
            forceParse: false,
            forceParse: false,
            autoclose: true,
            todayHighlight: true
        });

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, {color: '#1AB394'});
        //
        // var elem_2 = document.querySelector('.js-switch_2');
        // var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });
        //
        // var elem_3 = document.querySelector('.js-switch_3');
        // var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });
        //
        // var elem_4 = document.querySelector('.js-switch_4');
        // var switchery_4 = new Switchery(elem_4, { color: '#f8ac59' });
        // switchery_4.disable();

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

        $('.demo1').colorpicker();

        // var divStyle = $('.back-change')[0].style;
        // $('#demo_apidemo').colorpicker({
        //     color: divStyle.backgroundColor
        // }).on('changeColor', function(ev) {
        //     divStyle.backgroundColor = ev.color.toHex();
        // });

        $('.clockpicker').clockpicker();

        $('input[name="daterange"]').daterangepicker();

        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {days: 60},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });

        $(".select2_demo_1").select2();
        $(".select2_demo_2").select2();
        $(".select2_demo_3").select2({
            placeholder: "Select a state",
            allowClear: true
        });


        $(".touchspin1").TouchSpin({
            buttondown_class: 'btn btn-white',
            buttonup_class: 'btn btn-white'
        });

        $(".touchspin2").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%',
            buttondown_class: 'btn btn-white',
            buttonup_class: 'btn btn-white'
        });

        $(".touchspin3").TouchSpin({
            verticalbuttons: true,
            buttondown_class: 'btn btn-white',
            buttonup_class: 'btn btn-white'
        });

        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160
        });


    });

    $('.chosen-select').chosen({width: "100%"});

    $("#ionrange_1").ionRangeSlider({
        min: 0,
        max: 5000,
        type: 'double',
        prefix: "$",
        maxPostfix: "+",
        prettify: false,
        hasGrid: true
    });

    $("#ionrange_2").ionRangeSlider({
        min: 0,
        max: 10,
        type: 'single',
        step: 0.1,
        postfix: " carats",
        prettify: false,
        hasGrid: true
    });

    $("#ionrange_3").ionRangeSlider({
        min: -50,
        max: 50,
        from: -25,
        to: 25,
        type: 'double',
        postfix: "°",
        prettify: false,
        hasGrid: true
    });

    $("#ionrange_4").ionRangeSlider({
        values: [
            "January", "February", "March",
            "April", "May", "June",
            "July", "August", "September",
            "October", "November", "December"
        ],
        type: 'single',
        hasGrid: true
    });

    $("#ionrange_5").ionRangeSlider({
        min: 10000,
        max: 100000,
        step: 100,
        postfix: " km",
        from: 55000,
        hideMinMax: true,
        hideFromTo: false
    });

    $(".dial").knob();

    // var basic_slider = document.getElementById('basic_slider');
    //
    // noUiSlider.create(basic_slider, {
    //     start: 40,
    //     behaviour: 'tap',
    //     connect: 'upper',
    //     range: {
    //         'min':  20,
    //         'max':  80
    //     }
    // });

    // var range_slider = document.getElementById('range_slider');
    //
    // noUiSlider.create(range_slider, {
    //     start: [ 40, 60 ],
    //     behaviour: 'drag',
    //     connect: true,
    //     range: {
    //         'min':  20,
    //         'max':  80
    //     }
    // });

    // var drag_fixed = document.getElementById('drag-fixed');
    //
    // noUiSlider.create(drag_fixed, {
    //     start: [ 40, 60 ],
    //     behaviour: 'drag-fixed',
    //     connect: true,
    //     range: {
    //         'min':  20,
    //         'max':  80
    //     }
    // });


</script>
