<!-- Mainly scripts -->
<script src="{{asset('admin/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('admin/js/popper.min.js')}}"></script>
<script src="{{asset('admin/js/bootstrap.js')}}"></script>

<!-- Flot -->
<script src="{{asset('admin/js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.spline.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.symbol.js')}}"></script>
<script src="{{asset('admin/js/plugins/flot/jquery.flot.time.js')}}"></script>

<!-- Peity -->
<script src="{{asset('admin/js/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('admin/js/demo/peity-demo.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('admin/js/inspinia.js')}}"></script>
<script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset('admin/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- GITTER -->
<script src="{{asset('admin/js/plugins/gritter/jquery.gritter.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset('admin/js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Sparkline demo data  -->
<script src="{{asset('admin/js/demo/sparkline-demo.js')}}"></script>

<!-- ChartJS-->
<script src="{{asset('admin/js/plugins/chartJs/Chart.min.js')}}"></script>

<!-- Toastr -->
<script src="{{asset('admin/js/plugins/toastr/toastr.min.js')}}"></script>

{{--steps--}}
<script src="{{asset('admin/js/plugins/steps/jquery.steps.min.js')}}"></script>

<!-- Data picker -->
<script src="{{asset('admin/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="{{asset('admin/js/plugins/fullcalendar/moment.min.js')}}"></script>

<!-- Date range picker -->
<script src="{{asset('admin/js/plugins/daterangepicker/daterangepicker.js')}}"></script>

<!-- Chosen -->
<script src="{{asset('admin/js/plugins/chosen/chosen.jquery.js')}}"></script>

<!-- JSKnob -->
<script src="{{asset('admin/js/plugins/jsKnob/jquery.knob.js')}}"></script>

<!-- NouSlider -->
<script src="{{asset('admin/js/plugins/nouslider/jquery.nouislider.min.js')}}"></script>

<!-- Switchery -->
<script src="{{asset('admin/js/plugins/switchery/switchery.js')}}"></script>

<!-- IonRangeSlider -->
<script src="{{asset('admin/js/plugins/ionRangeSlider/ion.rangeSlider.min.js')}}"></script>

<!-- iCheck -->
<script src="{{asset('admin/js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>

<!-- MENU -->
<script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>

<!-- Color picker -->
<script src="{{asset('admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>

<!-- Clock picker -->
<script src="{{asset('admin/js/plugins/clockpicker/clockpicker.js')}}"></script>

<!-- Image cropper -->
<script src="{{asset('admin/js/plugins/cropper/cropper.min.js')}}"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="{{asset('admin/js/plugins/fullcalendar/moment.min.js')}}"></script>

<!-- Date range picker -->
<script src="{{asset('admin/js/plugins/daterangepicker/daterangepicker.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>

<!-- TouchSpin -->
<script src="{{asset('admin/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

<!-- Tags Input -->
<script src="{{asset('admin/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

<!-- Dual Listbox -->
<script src="{{asset('admin/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}"></script>


