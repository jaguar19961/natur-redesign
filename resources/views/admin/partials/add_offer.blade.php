@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Adaugare oferta noua</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@addNewOffers','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-md-8">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Seo - Descriere</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="descr[{{$localeCode}}]" required>
                                </div>
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Keywords {{$localeCode}}</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="keywords[{{$localeCode}}]" required>
                                </div>
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Denumire {{$localeCode}}</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="name[{{$localeCode}}]" required>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine din oferta</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image_offer" required/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Adauga</button>
                        </div>
                    </div>
                    <div class="row">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <div class="col-md-6">
                                <h3>Content {{$localeCode}}</h3>
                                <textarea class="form-control my-editor" name="content[{{$localeCode}}]" id="content" rows="20"></textarea>
                            </div>
                        @endforeach
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection