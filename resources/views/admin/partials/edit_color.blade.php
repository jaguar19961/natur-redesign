@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>editare culori</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addNewColor','files'=>true)) }}
                {{ csrf_field() }}

                <div class="row" style="padding-bottom: 20px">
                    <div class="col-md-6">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Denumire</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Adaugare culoare</span>
                            </div>
                            <input type="color" class="form-control demo1 colorpicker-element" name="code" value="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Adauga</button>
                    </div>
                </div>
                {{Form::close()}}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Denumire Culoare</th>
                        <th>Cod Culoare</th>
                        <th>Imagine Culoare</th>
                        <th>Salveaza</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($colors))
                        @foreach($colors as $item)
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@addColorImage','files'=>true)) }}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$item->id}}" name="id">
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td style="width: 10%"><div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon" style="background-color: {{$item->code}}; color: white">{{$item->code}}</span>
                                        </div>
                                        <input type="color" class="form-control demo1 colorpicker-element" name="name" value="{{$item->code}}">
                                    </div></td>
                                <td>
                                    @if(empty($item->image))
                                    <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                    </div>
                                        @else
                                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                        </div>
                                        <img src="{{asset('images/catalog/'.$item->image)}}" style="max-width: 100px;">
                                    @endif
                                </td>
                                <td style="width: 20%;">

                                    <button type="submit" class="btn btn-primary">Salveaza</button>

                                    {{--<a href="{{url('admin/edit_color/delete/'.$item->id)}}" class="btn btn-danger">Sterge</a>--}}
                                </td>
                            </tr>
                            {{Form::close()}}
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection