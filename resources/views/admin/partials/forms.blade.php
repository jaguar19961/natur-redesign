@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Formulare</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Denumire formular</th>
                            <th>Text</th>
                            <th>Actiune</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($forms as $form)
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@updateForm')) }}
                            {{ csrf_field() }}
                            <input type="hidden" name="form_id" value="{{$form->id}}">
                            <tr>
                                <td>{{$form->id}}</td>
                                <td>{{$form->form_name}}</td>
                                <td><input name="name" class="form-control" type="text" value="{{$form->name}}"></td>
                                <td>
                                    <button class="btn btn-xs btn-success" type="submit">Salveaza</button>
                                </td>
                            </tr>
                            {{Form::close()}}
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection