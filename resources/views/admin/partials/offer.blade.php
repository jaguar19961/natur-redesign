@extends('admin.layouts.app')
@section('content')
    <style>
        .product-box {
            padding: 0;
            border: 1px solid #e7eaec;
            width: 302px !important;
            height: 393px !important;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
            max-width: 300px !important;
        }

        .form-offer form{
            display: flex;
        }
    </style>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Oferte</h2>

        </div>
        <div class="col-lg-2">
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <a href="{{url('admin/add_offer')}}" class="btn btn-primary">Adugare postare</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($offers as $offer)
            <div class="col-md-3">
                <div class="ibox">
                    <div class="ibox-content product-box">
                        <img src="{{asset('images/article/'.$offer->image)}}" style="max-width: 300px;
    width: 100%;
    height: 163px;">

                        <div class="product-desc">
                            <a href="#" class="product-name"> {{  str_limit($offer->lang->name, 50) }}</a>

                            {{--<div class="small m-t-xs">--}}
                            {{--{!! str_limit( $offer->lang->description, 50) !!}--}}
                            {{--</div>--}}
                            <div class="col-md-12" style="margin-top: 10px">
                                <a target="_blank" href="{{url('/offers/article/'.$offer->lang->slug .'/'.$offer->id)}}"
                                   class="btn btn-xs btn-outline btn-primary">Oferta<i
                                            class="fa fa-long-arrow-right"></i> </a>
                                <a href="{{url('admin/edit-offer/'. $offer->id)}}"
                                   class="btn btn-xs btn-outlin btn-info">Editare <i class="fa fa-long-arrow-right"></i></a>
                                <a href="{{url('admin/delete_offer/'. $offer->id)}}"
                                   class="btn btn-xs btn-outline btn-danger">Sterge <i
                                            class="fa fa-long-arrow-right"></i> </a>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                @if($offer->publicata == 0)
                                    <a href="{{url('admin/offer-publication/'. 1 . '/'.$offer->id)}}"
                                       class="btn btn-xs btn-success">Publica</a>
                                @else
                                    <a href="{{url('admin/offer-publication/'. 0 . '/'.$offer->id)}}"
                                       class="btn btn-xs btn-info">Opreste publicarea</a>
                                @endif
                                <hr>
                            </div>
                            <div class="col-md-12 form-offer" style="display: flex">
                                {{ Form::open(array('method' => 'put','action' => 'AdminController@selectForm')) }}
                                {{ csrf_field() }}
                                <input type="hidden" name="offer_id" value="{{$offer->id}}">
                                <select class="form-control" name="form" id="">
                                    <option value="0">Nici un formular nu este selectat</option>
                                    @foreach($forms as $form)
                                        <option @if($offer->form_id == $form->id) selected @endif value="{{$form->id}}">{{$form->form_name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-xs btn-success">Salveaza</button>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
