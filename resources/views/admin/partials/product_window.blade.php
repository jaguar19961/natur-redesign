@extends('admin.layouts.app')
@section('content')
    <div class="col-md-12 justify-content-center text-center">
        <img src="{{asset('/images/catalog/'.$getImageColorDoor->image)}}" alt="" style="max-height: 100px;">
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Adaugare insertii (ferestre) pentru [Usa - @if (!empty($product->name)){{$product->name}} @endif, <span style="color: red;">CULOAREA - @if (!empty($getImageColorDoor->color->name)) {{$getImageColorDoor->color->name}} @endif</span>],</h5>
                <span>Daca nu este disponibila insertia(sticla) accesati acest link pentru adaugare - <a href="{{ url('/admin/edit_window') }}" class="btn btn-xs btn-danger">Adauga!</a></span>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addProductWindowWindow','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$idProduct}}">
                <input type="hidden" name="color_id" value="{{$idColor}}">
               <div class="row" style="padding-bottom: 20px">
                   <div class="col-md-10">
                       <select multiple class="form-control"  name="window_id[]" id="id1_1">
                           <option disabled>Alege...</option>


                           @foreach($windows as $window)

                                       <option @foreach($productWindows as $oldWin) @if($window->id == $oldWin->window_id) disabled @endif  @endforeach value="{{$window->id}}">{{$window->name}}</option>

                           @endforeach

                       </select>
                   </div>
                   <div class="col-md-2">
                       <button type="submit" class="btn btn-primary">Adauga</button>
                   </div>
               </div>
                {{Form::close()}}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Denumire Insertie</th>
                        <th>Imagine Produs</th>
                        <th>Salveaza</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($productWindows))
                    @foreach($productWindows as $item)
                        {{ Form::open(array('method' => 'put','action' => 'AdminController@addProductWindowImage','files'=>true)) }}
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$item->id}}" name="id">
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>
                                @if(!empty($item->window->image))
                                    <img src="{{asset('images/catalog/'.$item->window->image)}}" style="max-height: 50px;">
                                @endif
                                {{$item->window->name}}
                            </td>
                            <td>
                                @if(!empty($item->image))
                                <img src="{{asset('images/catalog/'.$item->image)}}" style="max-height: 150px;">
                                @endif
                                <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required/></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                            </td>
                            <td><button type="submit" class="btn btn-primary">Salveaza</button>
                                <a href="{{url('/admin/delete-window-product/'. $item->id)}}" class="btn btn-danger">Sterge</a> </td>
                        </tr>
                        {{Form::close()}}
                    @endforeach
                        @endif
                    </tbody>
                </table>



            </div>
        </div>
    </div>



@endsection