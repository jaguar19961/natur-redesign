@extends('admin.layouts.app')
@section('content')

    <style>
        .product-box {
            padding: 0;
            border: 1px solid #e7eaec;
            width: 302px !important;
            height: 337px !important;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
            max-width: 300px !important;
        }
    </style>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Blog</h2>

        </div>
        <div class="col-lg-2">
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <a href="{{url('admin/add_blog')}}" class="btn btn-primary">Adaugare postare</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row p-t-3">
        @foreach($systems as $blog)
            <div class="col-md-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <a target="_blank" href="{{url('/sisteme-de-deschidere/afisare/'.$blog->slug .'/'.$blog->id)}}" class="btn-link">
                            <img src="{{asset('images/systems/'.$blog->image)}}" style="max-width: 300px;
    width: 100%;
    height: 163px;">
                            <h4>
                                {{$blog->name}}
                            </h4>
                        </a>
                        <div class="row">
                            <div class="col-md-12">
                                {{--<h5>Tags: Usi de interior, Usi</h5>--}}
                                <a href="{{url('/sisteme-de-deschidere/afisare/'.$blog->slug .'/'.$blog->id)}}"
                                   class="btn btn-primary btn-xs" target="_blank">Articol</a>
                                <a href="{{url('admin/open-edit-open-system/'.$blog->id)}}"
                                   class="btn btn-primary btn-xs">Editare</a>
                                <a href="{{url('admin/delete-open-system/'. $blog->id)}}"
                                   class="btn btn-danger btn-xs">Sterge</a>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>




@endsection