@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Editare produs {{$select->name}}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@productPrice','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$select->id}}">
                <input type="hidden" name="cat_id" value="{{$select->categories->id}}">

                <div class="input-group">

                        <select class="form-control m-b" name="style_id">
                            @if($select->style_id != null || $select->style_id != 0)
                                @foreach($styles as $style)
                                    <option @if($select->style_id == $style->id) selected @endif value="{{$style->id}}">{{$style->name}}</option>
                                @endforeach
                            @else
                                <option selected disabled>Stil</option>
                                @foreach($styles as $style)
                                    <option value="{{$style->id}}">{{$style->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <select class="form-control m-b" name="type_id">
                            @if($select->type_id != null || $select->type_id != 0)
                                @foreach($types as $type)
                                    <option @if($select->type_id == $type->id) selected @endif value="{{$type->id}}">{{$type->type}}</option>
                                @endforeach
                            @else
                                <option selected disabled>Tip</option>
                                @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                @endforeach
                            @endif
                        </select>

                    <div class="input-group-prepend">
                        <span class="input-group-addon">Pret</span>
                    </div>
                    <input type="text" placeholder="" class="form-control" name="price" value="{{$select->price}}">
                    <button type="submit" class="btn btn-primary btn-xs">Salveaza</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Editare produs {{$select->name}}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@editProductDescription','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$select->id}}">
                <input type="hidden" name="serie_id" value="{{$select->serie_id}}">
                <div class="row" style="margin-top: 20px">

                    <div class="col-md-6">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Seo Keywords</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="keywords" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Seo Description</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="desc" required>
                        </div>
                    </div>
                        <div class="col-md-6">
                            <h3 class="p-4">Editare text set standard text scurt</h3>
                            <textarea class="form-control my-editor" name="description" id="content" rows="20">{{$select->description}}</textarea>
                        </div>
                    <div class="col-md-6">
                        <h3 class="p-4">Editare text set standard citeste mai mult</h3>
                        <textarea class="form-control my-editor" name="complect_description" id="content" rows="20">{{$select->complect_description}}</textarea>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Salveaza modificari</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection