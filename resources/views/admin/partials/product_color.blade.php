@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Adaugare culoare produse</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addProductColorColor','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$idProduct}}">
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-md-10">
                        <select multiple class="form-control" name="colors[]" id="id1_1">
                            <option disabled>Alege...</option>
                            @foreach($colors as $color)
                                <option @foreach($productColor as $oldWin) @if($color->id == $oldWin->color_id) disabled
                                        @endif  @endforeach value="{{$color->id}}">{{$color->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Adauga</button>
                    </div>
                </div>
                {{Form::close()}}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Denumire Culoare</th>
                        <th>Imagine Produs</th>
                        <th>Adaugare sticla</th>
                        <th>Salveaza</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($productColor))
                        @foreach($productColor as $item)
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@addProductColorImage','files'=>true)) }}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$item->id}}" name="id">
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->color->name}}
                                    @if(!empty($item->color->image))
                                        <img src="{{asset('images/catalog/'.$item->color->image)}}"
                                             style="max-height: 50px;">
                                    @endif

                                </td>
                                <td>
                                    @if(!empty($item->image))
                                        <img src="{{asset('images/catalog/'.$item->image)}}" style="max-height: 150px;">
                                    @endif
                                    <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required/></span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                           style="float: none">×</a>
                                    </div>
                                </td>
                                <td><a target="_blank" href="{{url('admin/add_product_window/'.$idProduct . '/' . $item->id)}}"
                                       class="btn btn-primary">Adauga sticle</a></td>
                                <td>
                                    <button type="submit" class="btn btn-primary">Salveaza</button>
                                    <a href="{{url('admin/add_product_color/delete/'. $item->id)}}"
                                       class="btn btn-danger">Sterge</a></td>
                            </tr>
                            {{Form::close()}}
                        @endforeach
                    @endif
                    </tbody>
                </table>


            </div>
        </div>
    </div>



@endsection
