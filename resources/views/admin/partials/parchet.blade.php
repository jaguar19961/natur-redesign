@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Adaugare parchet</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addProduct','files'=>true)) }}
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control m-b" name="category" id="category_id">
                            <option disabled selected>Alege Categorie</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->lang->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="col-md-2">--}}
                        {{--<select class="form-control m-b" name="serie_id" id="subcategory_id">--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    <div class="col-md-2">
                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                        </div>
                    </div>
                    {{--<div class="col-md-3">--}}
                        {{--<select multiple class="form-control"  name="colors[]" id="id1_1">--}}
                            {{--<option disabled>Alege...</option>--}}
                            {{--@foreach($colors as $color)--}}
                                {{--<option value="{{$color->id}}">{{$color->name}}</option>--}}
                            {{--@endforeach--}}

                        {{--</select>--}}
                    {{--</div>--}}
                    <div class="col-md-3">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Denumire parchet</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Pret</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="price">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Seo Keywords</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="keywords" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Seo Description</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="desc" required>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <hr>
                    <div class="col-md-12">
                        <h3>Specificatii parchet</h3>
                        <textarea class="form-control my-editor" name="content1" id="content" rows="20"></textarea>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Adauga produs</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <script>
        $('#category_id').on('change', function () {
            getProducts($(this).val());
        });
        function getProducts(category_id) {
            $.get("{{url('/admin/category/sub')}}/" + category_id, function (data) {
                $("#subcategory_id").html(data);
            });
        }
        $(document).ready(function () {
            getProducts($('#category_id').val());
        });
    </script>



@endsection