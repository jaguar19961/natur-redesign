@extends('admin.layouts.app')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Editare produse</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">


                @foreach($category as $item)
                    <a href="{{url('admin/category/'.$item->id)}}" class="btn btn-primary">{{$item->langs->name}}</a>
                @endforeach
                <br>
                <br>
                @if(!empty($getSeries))
                    @foreach($getSeries as $series)
                        <a href="{{url('admin/category/series/'.$series->category_id. '/'.$series->id)}}"
                           class="btn btn-primary">{{$series->name}}</a>
                    @endforeach
                @endif
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Categorie</th>
                        @if(!empty($product->serie))<th>Serie</th>@endif
                        <th>Nume</th>
                        <th>Editare informatie</th>
                        <th>Vezi Produs</th>
                        <th>Actiune</th>
                        <th>Info. aditionala</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $product)
                        <tr>
                            <td>{{$product->categories->langs->name}}</td>
                            @if(!empty($product->serie))<td>{{$product->serie->name}}</td>@endif
                            <td>{{$product->name}}</td>
                            <td><a target="_blank" href="{{url('admin/editProductSelected/'.$product->id)}}" class="btn btn-primary btn-xs" >
                                   Editare informatie
                                </a></td>
                            <td>
                                <a target="_blank" href="{{url('/product/'. $product->name . '/' . $product->id)}}" class="btn btn-xs btn-primary">Treci la produs</a>
                            </td>
                            <td style="width: 20%">
                                @if($product->categories->id == 1 || $product->categories->id == 2)
                                <a href="{{url('admin/add_product_color/'.$product->id)}}" target="_blank"
                                   class="btn btn-info btn-xs">Culori</a>
                                @endif
                                <a href="{{url('admin/edit_product/delete/'.$product->id)}}"
                                   class="btn btn-danger btn-xs">Sterge</a>
                            </td>
                        </tr>


                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection