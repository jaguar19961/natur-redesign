@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Editare articol</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            {{--<li><a href="#" class="dropdown-item">Config option 1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#" class="dropdown-item">Config option 2</a>--}}
                            {{--</li>--}}
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@putEditBlog','files'=>true)) }}
                    {{ csrf_field() }}
                    <input name="id" value="{{$blog->id}}" type="hidden">
                    <div class="row" style="padding-bottom: 20px">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <div class="col-md-6">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Nume</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="name[{{$localeCode}}]" required value="{{$blog->lang->name}}">
                                </div>
                        </div>
                            <div class="col-md-6">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Seo - Descriere</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="descr[{{$localeCode}}]" required value="{{$blog->lang->descr}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Seo - Keywords</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="keywords[{{$localeCode}}]" required value="{{$blog->lang->keywords}}">
                                </div>
                            </div>

                        @endforeach
                            <div class="col-md-6">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Pozitie</span>
                                    </div>
                                    <input type="number" placeholder="" class="form-control" name="position" required value="{{$blog->position}}">
                                </div>
                            </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Adauga</button>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('images/article/'.$blog->image)}}" style="max-height: 100px">
                        </div>
                    </div>
                    <div class="row">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <div class="col-md-12">
                                <h3>Content</h3>
                                <textarea class="form-control my-editor" name="content[{{$localeCode}}]" id="content" rows="20">{{$blog->lang->description}}</textarea>
                            </div>
                            @endforeach
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection