@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Editare oferta - {{$offer->lang->name}}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@putEditOffer','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-md-8">
                            <input type="hidden" name="id" value="{{$offer->id}}">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Seo - Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="descr" required
                                       value="{{$offer->lang->descr}}">
                            </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Keywords</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="keywords" required
                                       value="{{$offer->lang->keywords}}">
                            </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Denumire</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="name" required
                                       value="{{$offer->lang->name}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/article/'.$offer->image)}}" alt="" style="max-width: 150px;">
                        </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine din oferta</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image_offer"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/article/'.$offer->image_offer)}}" alt="" style="max-width: 150px;">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Adauga</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Content</h3>
                            <textarea class="form-control my-editor" name="content" id="content"
                                      rows="20">{{$offer->lang->description}}</textarea>
                        </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection