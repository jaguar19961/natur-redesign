@extends('admin.layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Categorii site</h5>

                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="accordion" id="accordionExample">
                    @foreach($menues->where('activity', 1) as $menue)
                    <div class="card">
                        <div class="card-header" id="headingOne{{$menue->id}}">
                            <h5 class="mb-0">
                                <button class="btn btn-primary btn-xs" type="button" data-toggle="collapse" data-target="#collapseOne{{$menue->id}}" aria-expanded="true" aria-controls="collapseOne{{$menue->id}}">
                                    {{$menue->name}}
                                </button>
                            </h5>
                            <h3>@if($menue->activity == 1) Meniu activ @else Inactiv @endif</h3>
                        </div>

                        <div id="collapseOne{{$menue->id}}" class="collapse" aria-labelledby="headingOne{{$menue->id}}" data-parent="#accordionExample">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nume</th>
                                        <th>Link</th>
                                        <th>Actiune</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($menue->dropdowns->where('divider', '!=', 1) as $dropdown)
                                        {{ Form::open(array('method' => 'put','action' => 'MenuEditorController@submenuSave','files'=>true)) }}
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$dropdown->id}}">
                                    <tr>
                                        <td>{{$dropdown->id}}</td>
                                        <td>{{$dropdown->name}}</td>
                                        <td><div class="input-group m-b">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-addon">Link</span>
                                                </div>
                                                <input name="link" type="text" placeholder="{{$dropdown->link}}" class="form-control">
                                            </div></td>
                                        <td> <button class="btn btn-primary" type="submit">Salveaza modificari</button> </td>
                                    </tr>
                                        {{Form::close()}}
                                   @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>


            </div>
        </div>
    </div>

</div>


@endsection